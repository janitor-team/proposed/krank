#!/usr/bin/bash

# dist_win [version] will produce krank-[version]-setup.exe from files in dist folder

rm -rf dist
rm -rf build
find levels -name "*.pyc" -exec rm {} \;
find src -name "*.pyc" -exec rm {} \;
python setup_win.py py2exe
mv dist/Main.exe dist/krank.exe
/cygdrive/c/Programme/Inno\ Setup\ 5/ISCC.exe krank.iss
rm -f Output/krank-$1-setup.exe
mv Output/krank-setup.exe Output/krank-$1-setup.exe