import k
from levels import *
from Part import *
from Effect import *
from Tools import *

def init():
    k.sound.loadTheme('menu')
    k.sound.music('water.ogg')
    k.world.setBackground('IHP01')
    # simple player
    k.player.setPos(k.world.rect.center)
    k.player.setTailNum(2)

    index = 0
    
    c, h = k.world.rect.centerx, k.world.rect.height
    drawText('Credits', (c, h*4.0/24.0), color=(0,0,0), valign='center')
    drawText('Code by monsterkodi', (c, h*6/28), color=(0,0,0), size='normal', valign='center')
    drawText('Sound by legoluft', (c, h*7/28), color=(0,0,0), size='normal', valign='center')    
    drawText('Images', (c, h*8/24), color=(0,0,0), valign='center')
    credits = ["Big-E-Mr-G", "darkmatter", "*GaryP*", "IHP", "kahanaboy", "nasirkhan", "OzBandit", "somadjinn", "tanakawho"] 

    index = 1
    for text in credits:
        
        drawText(text, (k.world.rect.centerx, h*(8/24.0+index/28.0)), size='normal', color=(0,0,0))
        index += 1
        
    drawText('Font by Nick Curtis', (c, h*21/28), color=(0,0,0), size='normal', valign='center')    
    drawText('Version ' + k.VERSION, (c, h*23/28), color=(220,209,180), size='tiny', valign='center')    
        
    k.particles.add(Switch({ 'text': 'Back',
                             'align': 'bottom',
                             'action': 'k.level.menuExit()',
                             'size': 'small', 
                             'pos': vector((c, h*7/8))}))