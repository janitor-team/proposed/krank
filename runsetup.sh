#!/bin/sh

rm -rf krank.app dist build

python setup.py py2app -A
mv dist/krank.app .
mv krank.app/Contents/MacOS/krank krank.app/Contents/MacOS/krank.hide
cp /Library/Frameworks/Python.framework/Versions/2.4/Resources/Python.app/Contents/MacOS/Python krank.app/Contents/MacOS/krank