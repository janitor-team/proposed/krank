#!/bin/sh

# dist [version] will produce krank-[version].dmg from files in dist folder

rm -rf dist/krank.app
rm -rf release/krank-$1.dmg
find levels -name "*.pyc" -exec rm {} \;
find src -name "*.pyc" -exec rm {} \;
python setup.py py2app
hdiutil create -format UDZO -uid 99 -gid 99 -volname krank -srcfolder dist release/krank-$1.dmg
hdiutil eject /Volumes/krank
open release/krank-$1.dmg
