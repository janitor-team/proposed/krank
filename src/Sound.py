#
# Sound.py

import k
from Krank import *

#-----------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------

class Sound (object):

    #-------------------------------------------------------------------------------------------
    def __init__(self):
        #log(log='startup')
        k.sound = self
        
        self.sounds = {}
        self.soundDir = None
        self.soundVolume = 1.0
        self.musicVolume = 1.0
        
        pygame.mixer.init()
        pygame.mixer.set_num_channels(32)
        log('num channels', pygame.mixer.get_num_channels() , log='sound')
        
    #-------------------------------------------------------------------------------------------
    def reset (self):        
        self.sounds = {}
        self.soundDir = None
        pygame.mixer.music.stop()
        pygame.mixer.stop()
        
    #-------------------------------------------------------------------------------------------
    def loadTheme (self, set='industry'):
        log('sound set', set, log='sound')
        self.soundDir = os.path.join('sounds', set)
        if os.path.exists(self.soundDir):
          for file in os.listdir(self.soundDir):
              if os.path.splitext(file)[1] in ['.wav']:
                  key = os.path.splitext(os.path.basename(file))[0]
                  #log('sound loaded:', key, '->', file, log='sound')
                  self.sounds[key] = pygame.mixer.Sound(os.path.join(self.soundDir, file))
                  self.sounds[key].set_volume(self.soundVolume)
        else:
          self.soundDir = None
          log('invalid sound set')
        
    #-------------------------------------------------------------------------------------------
    def play (self, sound, volume=1.0, force=0, event=None):
        if volume > 0 and self.soundVolume > 0:
            log(sound, volume, force, event, log='sound')
            volume = clamp(volume, 0.1, 1.0)
            if self.sounds.has_key(sound):
                if force:
                    channel = pygame.mixer.find_channel(1)
                    channel.play(self.sounds[sound])
                else:
                    channel = self.sounds[sound].play()
                if channel:
                    channel.set_volume(volume)
                    if event:
                        channel.set_endevent(event)
                else:
                    log('no channel', log='sound')
            
    #-------------------------------------------------------------------------------------------
    def removeEndEvent (self, type):
        for i in range(pygame.mixer.get_num_channels()):
            channel = pygame.mixer.Channel(i)
            if channel.get_endevent() == type:
                channel.set_endevent()
            
    #-------------------------------------------------------------------------------------------
    def music (self, name='atmo.ogg'):
        if self.soundDir:
          musicFile = os.path.join(self.soundDir, name)
          if os.path.exists(musicFile):
            pygame.mixer.music.load(os.path.join(self.soundDir, name))
            if self.musicVolume > 0:
                pygame.mixer.music.play(-1)
            self.setMusicVolume(self.musicVolume)
        
    #-------------------------------------------------------------------------------------------
    def setMusicVolume (self, volume):
        self.musicVolume = clamp(volume, 0.0, 1.0)
        pygame.mixer.music.set_volume(self.musicVolume)
        if self.musicVolume > 0:
            if not pygame.mixer.music.get_busy():
                pygame.mixer.music.play(-1)
        else:
            if not pygame.mixer.music.get_busy():
                pygame.mixer.music.stop()
        
    #-------------------------------------------------------------------------------------------
    def getMusicVolume (self):
        return pygame.mixer.music.get_volume()

    #-------------------------------------------------------------------------------------------
    def setMusicVolumeIndex (self, index):
        index = clamp(index, 0, 5)
        self.setMusicVolume (index*index/25.0)
        
    #-------------------------------------------------------------------------------------------
    def getMusicVolumeIndex (self):
        v = pygame.mixer.music.get_volume()
        for i in range(6):
            if abs(v-i*i/25.0) < 0.01: return i
        return 5
        
    #-------------------------------------------------------------------------------------------
    def setSoundVolume (self, volume):
        log(volume, log='sound')
        play = self.soundVolume <> clamp(volume, 0.0, 1.0)
        self.soundVolume = clamp(volume, 0.0, 1.0)
        for key in self.sounds.keys():
            self.sounds[key].set_volume(self.soundVolume)
        if play:
            k.sound.play('part')
        
    #-------------------------------------------------------------------------------------------
    def getSoundVolume (self):
        return self.soundVolume
    
    #-------------------------------------------------------------------------------------------
    def setSoundVolumeIndex (self, index):
        index = clamp(index, 0, 5)
        self.setSoundVolume (index*index/25.0)
        
    #-------------------------------------------------------------------------------------------
    def getSoundVolumeIndex (self):
        v = self.soundVolume
        for i in range(6):
            if abs(v-i*i/25.0) < 0.01: return i
        return 5
        