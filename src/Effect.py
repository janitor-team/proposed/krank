#
#  Part.py

import k

from Krank  import *
from Math   import *
from Sprite import *

#-----------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------

class Effects:

    #-------------------------------------------------------------------------------------------
    def __init__ (self):
        #log(log='startup')
        self.effects = []
        k.effects = self
        
        self.sparkImages = []
        self.sparkShades = 10
        for i in range(self.sparkShades):
            self.sparkImages.append(pygame.image.load('levels/images/spark.png'))
            array = pygame.surfarray.pixels_alpha(self.sparkImages[-1])
            for x in range(8):
                for y in range(8):
                    array[x][y] = max(0, array[x][y]-128.0*i/self.sparkShades)
        
    #-------------------------------------------------------------------------------------------
    def reset (self):
        self.effects = []
                
    #-------------------------------------------------------------------------------------------
    def add (self, item):
        self.effects.append(item)
            
    #-------------------------------------------------------------------------------------------
    def remove (self, item):
        self.effects.remove(item)
        
    #-------------------------------------------------------------------------------------------
    def onFrame (self, delta):
        for effect in self.effects:
          effect.onFrame(delta)
          
#-----------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------

class Spark:
    
    #-------------------------------------------------------------------------------------------
    def __init__ (self, dict={}):
        
        self.pos = vector(dict.get('pos'))
        self.vel = vector(dict.get('vel'))
                
        self.sprite = Sprite(self.pos, k.effects.sparkImages[0])
        k.effect_sprites.add(self.sprite)
                
    #-------------------------------------------------------------------------------------------
    def onFrame (self, delta):
        self.pos += self.vel * delta
        self.sprite.setPos(self.pos)
        
    #-------------------------------------------------------------------------------------------
    def onExit (self):
        self.sprite.kill()

#-----------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------

class SparkGroup:
  
    #-------------------------------------------------------------------------------------------
    def __init__ (self, pos, num=80):
        
        if num == 0: return
        
        dict = {}
        dict['pos']   = pos
        
        self.age      = 0
        self.max_age  = dict.get('max_age', 500)
        
        self.sparks   = []
        
        maxsparks = min(int(k.clock.get_fps()/5), 80)
        for i in range(min(num, maxsparks)):
          angle = random.random()*math.pi*2
          dict['vel'] = vector.withAngle(angle, random.random()*0.3)
          self.sparks.append(Spark(dict))
          
        k.effects.add(self)
                                
    #-------------------------------------------------------------------------------------------
    def onFrame (self, delta):
        self.age += delta
        
        if self.age > self.max_age:
            for spark in self.sparks:
                spark.onExit()
            self.sparks = []
            k.effects.remove(self)
        else:
            agefac = float(self.age)/self.max_age
            for spark in self.sparks:
                spark.sprite.image = k.effects.sparkImages[int(agefac*(k.effects.sparkShades-1))]
                spark.onFrame(delta)
  
