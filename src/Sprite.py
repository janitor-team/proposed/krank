#
#  Sprite.py

import k

from Krank  import *
from Math   import *

#-----------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------

class Sprite (pygame.sprite.Sprite):

    images = {}
    
    #-------------------------------------------------------------------------------------------    
    def __init__(self, pos, image, sprites=None):
        
        pygame.sprite.Sprite.__init__(self)

        if image.__class__ == pygame.Surface:
            self.image = image
        else:
            if Sprite.images.get(image) is None:
                Sprite.images[image] = pygame.image.load(image)
            self.image = Sprite.images[image]

        self.rect = self.image.get_rect()
        self.setPos(pos)
        if sprites <> None:
          sprites.add(self)
        else:
          k.sprites.add(self)
        
    #-------------------------------------------------------------------------------------------    
    def setPos (self, pos):
        try:
            self.rect.center = pos
        except Exception, e:
            self.rect.center = (clamp(pos.x, 0, k.world.rect.width), clamp(pos.y, 0, k.world.rect.height))
                                
        