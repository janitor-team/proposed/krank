#
# Krank.py

#-----------------------------------------------------------------------------------------------
# imports

import sys, os.path, dircache, math, sets, re, sets, shutil, time, copy, cPickle, pickle, random
import pygame

kNEXT_LEVEL, kMENU_LEVEL, kLOAD_LEVEL = range(pygame.USEREVENT, pygame.USEREVENT+3)

from Math import *

#-----------------------------------------------------------------------------------------------
# logging

_log_ = []

def log(*args, **kwargs):
    if not _log_: return
    level = kwargs.get('level', 1)
    catjust, clsjust, mthjust = 8 ,23, 22
    if "log" in kwargs:
        if kwargs["log"] not in _log_:
            return
        print kwargs["log"].rjust(catjust),
    else:
        print " ".rjust(catjust),
    print "[", 
    mth = sys._getframe(level).f_code.co_name
    try:
        obj = sys._getframe(level).f_locals['self']
        cls = hasattr(obj.__class__, 'className') and obj.__class__.className(obj) or str(obj.__class__.__name__)
        print cls.ljust(clsjust), mth.ljust(mthjust),
    except Exception, e:
        try:
            file = sys._getframe(level).f_globals['__file__']
            moduleName = os.path.join(os.path.split(os.path.dirname(file))[-1], os.path.basename(file))
            print moduleName.ljust(clsjust), mth.ljust(mthjust),
        except:
            print "???".ljust(clsjust), mth.ljust(mthjust),
    print "]",
    for i in args: print i,
    print ""

#-----------------------------------------------------------------------------------------------

def error(*args, **kwargs):
    kwargs['level'] = 2
    log("[ERROR]", *args, **kwargs)

